/*
 *   Copyright (c) 2021 László Hegedűs
 *   All rights reserved.
 */
module.exports = {
  stories: ['../dist/stories/**/*.stories.{js,md,mdx}'],
};
