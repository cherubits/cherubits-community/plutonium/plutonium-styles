/*
 *   Copyright (c) 2021 László Hegedűs
 *   All rights reserved.
 */
import '../plutonium-paper-styles.js';

import { TemplateResult, html } from 'lit-html';

export default {
  title: 'Plutonium / Theming',
  component: 'plutonium-styles',
  argTypes: {
    title: { control: 'text' },
    counter: { control: 'number' },
    textColor: { control: 'color' },
  },
};

interface Story<T> {
  (args: T): TemplateResult;
  args?: Partial<T>;
  argTypes?: Record<string, unknown>;
}

interface ArgTypes {
  title?: string;
  counter?: number;
  textColor?: string;
  slot?: TemplateResult;
}


const ThemeTemplate: Story<ArgTypes> = ({
  title = 'Hello world',
  counter = 5,
  textColor,
  slot,
}: ArgTypes) => html`
<h1>Headline 1</h1>
<h2>Headline 2</h2>
<h3>Headline 3</h3>
<h4>Headline 4</h4>
<h5>Headline 5</h5>
<h6>Headline 6</h6>
<div class="subtitle1">Subtitle 1</div>
<div class="subtitle2">Subtitle 2</div>
<div class="body1">Body 1. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quos blanditiis tenetur unde suscipit, quam beatae rerum inventore consectetur, neque doloribus, cupiditate numquam dignissimos laborum fugiat deleniti? Eum quasi quidem quibusdam.</div>
<div class="body1">Body 2. Lorem ipsum dolor sit amet consectetur adipisicing elit. Cupiditate aliquid ad quas sunt voluptatum officia dolorum cumque, possimus nihil molestias sapiente necessitatibus dolor saepe inventore, soluta id accusantium voluptas beatae.</div>
<button>BUTTON TEXT</button>
<caption>Caption text</caption>
<label>OVERLINE TEXT</label>

`;

export const DefaultTheming = ThemeTemplate.bind({});

export const SimpleTheming = ThemeTemplate.bind({});

export const StyledElements = ThemeTemplate.bind({});

export const BuildingThemes = ThemeTemplate.bind({});
