/*
 *   Copyright (c) 2021 László Hegedűs
 *   All rights reserved.
 */
import { css, unsafeCSS } from 'lit-element'
import {
  primaryBackgroundColor,
  primaryBorderColor,
  primaryColor,
  primaryDarkColor,
  primaryLightColor,
  primaryTextColor,
  secondaryBackgroundColor,
  secondaryBorderColor,
  secondaryColor,
  secondaryDarkColor,
  secondaryLightColor,
  secondaryTextColor
} from './PlutoniumColors.js'

/*
 *   Copyright (c) 2021 László Hegedűs
 *   All rights reserved.
 */


export const fontStyle = 'Roboto,sans-serif';

export const themeStyles = css`
  --plutonium-primary-background-color: ${unsafeCSS(primaryBackgroundColor)};
  --plutonium-primary-border-color: ${unsafeCSS(primaryBorderColor)};
  --plutonium-primary-color: ${unsafeCSS(primaryColor)};
  --plutonium-primary-dark-color: ${unsafeCSS(primaryDarkColor)};
  --plutonium-primary-light-color: ${unsafeCSS(primaryLightColor)};
  --plutonium-primary-text-color: ${unsafeCSS(primaryTextColor)};

  --plutonium-secondary-background-color: ${unsafeCSS(secondaryBackgroundColor)};
  --plutonium-secondary-border-color: ${unsafeCSS(secondaryBorderColor)};
  --plutonium-secondary-color: ${unsafeCSS(secondaryColor)};
  --plutonium-secondary-dark-color: ${unsafeCSS(secondaryDarkColor)};
  --plutonium-secondary-light-color: ${unsafeCSS(secondaryLightColor)};
  --plutonium-secondary-text-color: ${unsafeCSS(secondaryTextColor)};

  --plutonium-font-style: ${unsafeCSS(fontStyle)};

  --mdc-theme-on-surface: ${unsafeCSS(primaryColor)};
  --mdc-theme-text-icon-on-background: ${unsafeCSS(primaryColor)};
  --mdc-theme-text-primary-on-background: ${unsafeCSS(primaryTextColor)};
  --mdc-theme-text-secondary-on-background: ${unsafeCSS(secondaryTextColor)};
  --mdc-theme-hint-on-background: ${unsafeCSS(primaryColor)};
`
