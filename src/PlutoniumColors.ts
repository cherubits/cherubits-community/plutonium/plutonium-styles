/*
 *   Copyright (c) 2021 László Hegedűs
 *   All rights reserved.
 */
import { css } from 'lit-element';

export const primaryBackgroundColor = css`#9e9e9e`;
export const primaryBorderColor = css`#9e9e9e`;
export const primaryColor = css`#f3e5f5`;
export const primaryDarkColor = css`#c0b3c2`;
export const primaryLightColor = css`#ffffff`;
export const primaryTextColor = css`#000000`;

export const secondaryBackgroundColor = css`#9e9e9e`;
export const secondaryBorderColor = css`#9e9e9e`;
export const secondaryColor = css`#7b1fa2`;
export const secondaryLightColor = css`#ae52d4`;
export const secondaryDarkColor = css`#4a0072`;
export const secondaryTextColor = css`#ffffff`;
