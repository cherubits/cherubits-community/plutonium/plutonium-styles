/*
 *   Copyright (c) 2021 László Hegedűs
 *   All rights reserved.
 */

import {
  floatingActionStyles,
  initialeStyles
} from './src/PlutoniumPaperStyles.js'
import {
  materialDesignComponentStyles,
  materialWebComponentStyles
} from './src/PlutoniumMaterialStyles.js'
import {
  plutoniumFlexAlignmentClasses,
  plutoniumFlexClasses,
  plutoniumFlexFactorClasses,
  plutoniumFlexReverseClasses,
  plutoniumHorizontalLayoutStyle,
  plutoniumPositioningClasses,
  plutoniumVerticalLayoutStyle
} from './src/PlutoniumFlexStyles.js'
import {
  primaryBackgroundColor,
  primaryBorderColor,
  primaryColor,
  primaryDarkColor,
  primaryLightColor,
  primaryTextColor,
  secondaryBackgroundColor,
  secondaryBorderColor,
  secondaryColor,
  secondaryDarkColor,
  secondaryLightColor,
  secondaryTextColor
} from './src/PlutoniumColors.js'

import { themeStyles } from './src/PlutoniumThemeStyles.js'
